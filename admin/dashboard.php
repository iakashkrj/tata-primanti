<?php
session_start();
$admin=$_SESSION['sid'];
if(empty($admin))
{
	header("location:index.php");
}
include("config.php");
?>
<html>
	<head>
		<title>Dashboard</title>
		<link href="boot/bootstrap.min.css" rel="stylesheet">
		<script src="boot/jquery.min.js"></script>
		<script src="boot/bootstrap.min.js"></script>
		<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
	</head>
	
	<body>
		<main>	
			<header>
				<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">The School of Digital Marketing</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      
      <li><a href="#"></a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Welcome : <?=$admin;?></a></li>
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
    </ul>
  </div>
</nav>
			</header>		
			
			<div>
				<aside class="col-sm-4">
					<div class="list-group">
					<a href="?page=cat" class="list-group-item active">Category</a>
				
			</div>
				</aside>
				
				<section class="col-sm-8">
						<?php
						if(!empty($_GET['page']))
						{
							switch($_GET['page'])
							{
								case 'ind' : include('dash.php');
											break;
								case 'cat' : include("category.php");
											break;
							
							}
						}						
						?>
				</section>						
			</div>	
		
		</main>
	</body>
</html>