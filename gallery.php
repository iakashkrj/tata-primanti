<!-- gallery section -->
<section class="our-gallery slideanim" id="gallery">
	<h3 class="text-center slideanim">Our Gallery</h3>
	<p class="text-center slideanim"></p>
	<div class="container">
		<div id="photoGallery-container">
		<img src="images/d1.jpg" height="300" width="300">&nbsp;&nbsp;&nbsp;&nbsp;
        <img src="images/d2.jpg"  height="300" width="300">&nbsp;&nbsp;&nbsp;&nbsp;
        <img src="images/d3.jpg"  height="300" width="300"><br><br>
        <img src="images/d4.jpg"  height="300" width="300">&nbsp;&nbsp;&nbsp;&nbsp;
        <img src="images/d5.jpg"  height="300" width="300">&nbsp;&nbsp;&nbsp;&nbsp;
        <img src="images/d6.jpg"  height="300" width="300"><br><br>
        
       
          
               </div>
        
 
    </div>
    
		</div>
	</div>
</section>
<!-- /gallery section -->
<p class="text-center slideanim"></p>
	<b>
	
  <div class="container">
  	<div class="jumbotron">
  		<div class="row" style="color:#333" style="font:bolder" style="font-size:500px">
			<div align="center" style="color:#333"><h1><b>AMENITIES<b> </h1></div>
			<br>
			<br>
				<div class="col-sm-3 col-md-4" >
				        <div class="panel-group">
				            <div class="panel panel-default">
				                <div class="panel-body" class="text-primary">Coffee Shop  </div>
				            </div>
				            <div class="panel panel-default">
				                <div class="panel-body">Drop Off Point</div>
				            </div>
				        
				            <div class="panel panel-default">
				                <div class="panel-body">Reflecting Pool </div>
				            </div>
				            <div class="panel panel-default">
				                <div class="panel-body">Green Wall </div>
				            </div>
				     
				            <div class="panel panel-default">
				                <div class="panel-body">Planter </div>
				            </div>
				            <div class="panel panel-default">
				                <div class="panel-body">Party Lawn</div>
				            </div>
				       
				            <div class="panel panel-default">
				                <div class="panel-body">Spine Walk </div>
				            </div>
				           </b>
     
					</div>
        			</div>
        			
        			<div class="col-sm-3 col-md-4" >
				        <div class="panel-group">
				            <div class="panel panel-default">
				                <div class="panel-body" class="text-primary">Kids Game Zone   </div>
				            </div>
				            <div class="panel panel-default">
				                <div class="panel-body">Cards Room</div>
				            </div>
				        
				            <div class="panel panel-default">
				                <div class="panel-body">Female / Male Spa </div>
				            </div>
				            <div class="panel panel-default">
				                <div class="panel-body">Live Kitchen</div>
				            </div>
				     
				            <div class="panel panel-default">
				                <div class="panel-body">Multi Cuisine Restaurant</div>
				            </div>
				            <div class="panel panel-default">
				                <div class="panel-body">Salon </div>
				            </div>
				       
				            <div class="panel panel-default">
				                <div class="panel-body">Party Chamber  </div>
				            </div>
				           </b>
     
					</div>
        			</div>
				
				<div class="col-sm-3 col-md-4" >
				        <div class="panel-group">
				            <div class="panel panel-default">
				                <div class="panel-body" class="text-primary">Guest Rooms – 18  </div>
				            </div>
				            <div class="panel panel-default">
				                <div class="panel-body">Business Centre </div>
				            </div>
				        
				            <div class="panel panel-default">
				                <div class="panel-body">Multi-functional Conference Rooms </div>
				            </div>
				            <div class="panel panel-default">
				                <div class="panel-body">Seating Lounge </div>
				            </div>
				     
				            <div class="panel panel-default">
				                <div class="panel-body">Jogging Trail </div>
				            </div>
				            <div class="panel panel-default">
				                <div class="panel-body">Badminton Court </div>
				            </div>
				       
				            <div class="panel panel-default">
				                <div class="panel-body">Indoor Hopscotch game </div>
				            </div>
				           </b>
     
					</div>
        			</div>
				
				
        			
        			
        			
        			
        			
        			
        			
			</div>
		      
        
        
        
           
        
        
         </div>
    </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>