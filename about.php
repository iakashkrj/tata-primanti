<!-- about section -->
<section class="about-us" id="about">

	
		<p class="text-center slideanim"></p>
	
    <div class="container">
  <div class="jumbotron">
   <div align="center" style="color:#333"> <h1>TATA PRIMANTI</h1> </div>
    <center><font size="+1" color="#000000"><b>  Luxury 3 & 4 BHK Starting Rs. 2.3 Cr.</font><br>
    <font size="+1"color="#000000">4 BHK Vertillas Starting Rs. 5.6 Cr.</b></font><center>
   
<br>
    <p>Imagine life in a place where tree-lined boulevards wind their way through lush, wooded parks. Where picturesque pergolas and water bodies, dappled with sunlight and shade, glimmer through the trees. Primanti is situated on the Southern Peripheral Road (SPR) in Sector 72, Gurugram. It is a residential development spreading over an area of 36 acres and offers both: apartments and villas. Primanti is designed around a series of interconnected orchards, meadows and gardens that span sinuously across the development. The rich flora forms dramatic patterns with stone structures and water features, inspired by Delhi’s Mughal gardens. Ensconced within this garden estate are premium apartments and duplexes, with courtyards, open terraces and private gardens. Green spaces meander from residential areas through manicured lawns to the luxurious clubhouse and spa, inviting residents to spend more time outdoors. Discover a home where modern architecture merges seamlessly with the natural landscape. A retreat for the senses, within the city of Gurugram.</p> 
  </div>
 
</div>
	
	
	
	<p class="text-center slideanim"></p>
	<div > 
		<div class="row">
			<div class="col-lg-12">
				<img src="images/1.jpeg"  style="width:100%;">
				<img src="images/scr.png" style="width:100%;">
			</div>
		</div>
	</div>
</section>
<!-- /about section -->


